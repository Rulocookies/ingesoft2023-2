from django.shortcuts import render, HttpResponse
from .models import *
from django.db.models import Count

# Create your views here.

def index(request):
    varGrupo1 = Estudiante.objects.filter(grupo = 1)
    varGrupo4 = Estudiante.objects.filter(grupo = 4)
    varMismoApellido = Estudiante.objects.filter(apellidos = 'Mendoza')
    varMismaEdad = Estudiante.objects.filter(edad = 24)
    varMismaEdadGrupo3 = Estudiante.objects.filter(edad = 22, grupo = 3)
    varTodos = Estudiante.objects.all()
    return render(request, 'index.html', {'varGrupo1':varGrupo1, 'varGrupo4':varGrupo4, 'varMismoApellido':varMismoApellido, 'varMismaEdad':varMismaEdad, 'varMismaEdadGrupo3':varMismaEdadGrupo3, 'varTodos':varTodos})
